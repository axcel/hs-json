module Lib
    ( someFunc
    ) where


import Data.Aeson
import Data.Text as T
import Data.ByteString.Lazy as B
import Data.ByteString.Lazy.Char8 as BC
import GHC.Generics

data Book = Book 
    {title :: T.Text
    , author :: T.Text
    , year :: Int
    } deriving (Show,Generic)


instance FromJSON Book
instance ToJSON Book    

myBook :: Book
myBook = Book {author="joe",title="title",year=2020}

myBookJSON :: BC.ByteString
myBookJSON = encode myBook

rawJSON :: BC.ByteString
rawJSON = "{\"year\":2010,\"author\":\"jabe\",\"title\":\"title\"}"

bookFromJSON :: Maybe Book
bookFromJSON = decode rawJSON

wrongJSON :: BC.ByteString
wrongJSON = "{\"year\":2010,\"writer\":\"jabe\",\"title\":\"title\"}"

bookFromWrongJSON :: Maybe Book
bookFromWrongJSON = decode wrongJSON

data ErrorMessage = ErrorMessage
    { message :: T.Text
    , errorCode :: Int
    } deriving Show

instance FromJSON ErrorMessage where
    parseJSON (Object v) =
        ErrorMessage <$> v .: "message"
                     <*> v .: "error"

instance ToJSON ErrorMessage where
    toJSON (ErrorMessage message errorCode) = 
        object [ "message" .= message
               , "error" .= errorCode
               ]

sampleError :: BC.ByteString
sampleError = "{\"message\":\"ooops!\",\"error\":123}"

sampleErrorMessage :: Maybe ErrorMessage
sampleErrorMessage = decode sampleError

anErrorMessage :: ErrorMessage
anErrorMessage = ErrorMessage "Not Found" 404

someFunc :: IO ()
someFunc = print "someFunc"
